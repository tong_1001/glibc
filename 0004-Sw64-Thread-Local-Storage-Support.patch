From 20affa072418646ada75247771c28633ac47688c Mon Sep 17 00:00:00 2001
From: swcompiler <lc@wxiat.com>
Date: Fri, 29 Nov 2024 13:55:34 +0800
Subject: [PATCH 04/23] Sw64: Thread-Local Storage Support

---
 sysdeps/sw_64/dl-tls.h        |  26 ++++++++
 sysdeps/sw_64/libc-tls.c      |  32 ++++++++++
 sysdeps/sw_64/nptl/tls.h      | 108 ++++++++++++++++++++++++++++++++++
 sysdeps/sw_64/stackinfo.h     |  33 +++++++++++
 sysdeps/sw_64/sw8a/nptl/tls.h | 106 +++++++++++++++++++++++++++++++++
 5 files changed, 305 insertions(+)
 create mode 100644 sysdeps/sw_64/dl-tls.h
 create mode 100644 sysdeps/sw_64/libc-tls.c
 create mode 100644 sysdeps/sw_64/nptl/tls.h
 create mode 100644 sysdeps/sw_64/stackinfo.h
 create mode 100644 sysdeps/sw_64/sw8a/nptl/tls.h

diff --git a/sysdeps/sw_64/dl-tls.h b/sysdeps/sw_64/dl-tls.h
new file mode 100644
index 00000000..0795a41a
--- /dev/null
+++ b/sysdeps/sw_64/dl-tls.h
@@ -0,0 +1,26 @@
+/* Thread-local storage handling in the ELF dynamic linker.  Sw_64 version.
+   Copyright (C) 2002-2023 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library.  If not, see
+   <http://www.gnu.org/licenses/>.  */
+
+/* Type used for the representation of TLS information in the GOT.  */
+typedef struct
+{
+  unsigned long int ti_module;
+  unsigned long int ti_offset;
+} tls_index;
+
+extern void *__tls_get_addr (tls_index *ti);
diff --git a/sysdeps/sw_64/libc-tls.c b/sysdeps/sw_64/libc-tls.c
new file mode 100644
index 00000000..f268dd08
--- /dev/null
+++ b/sysdeps/sw_64/libc-tls.c
@@ -0,0 +1,32 @@
+/* Thread-local storage handling in the ELF dynamic linker.  Sw_64 version.
+   Copyright (C) 2003-2023 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library.  If not, see
+   <http://www.gnu.org/licenses/>.  */
+
+#include <csu/libc-tls.c>
+#include <dl-tls.h>
+
+/* On Sw_64, linker optimizations are not required, so __tls_get_addr
+   can be called even in statically linked binaries.  In this case module
+   must be always 1 and PT_TLS segment exist in the binary, otherwise it
+   would not link.  */
+
+void *
+__tls_get_addr (tls_index *ti)
+{
+  dtv_t *dtv = THREAD_DTV ();
+  return (char *) dtv[1].pointer.val + ti->ti_offset;
+}
diff --git a/sysdeps/sw_64/nptl/tls.h b/sysdeps/sw_64/nptl/tls.h
new file mode 100644
index 00000000..8df86916
--- /dev/null
+++ b/sysdeps/sw_64/nptl/tls.h
@@ -0,0 +1,108 @@
+/* Definition for thread-local data handling.  NPTL/Sw_64 version.
+   Copyright (C) 2003-2023 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library.  If not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#ifndef _TLS_H
+#define _TLS_H 1
+
+#include <dl-sysdep.h>
+
+#ifndef __ASSEMBLER__
+#  include <stdbool.h>
+#  include <stddef.h>
+#  include <stdint.h>
+#  include <dl-dtv.h>
+
+/* Get system call information.  */
+#  include <sysdep.h>
+
+/* The TP points to the start of the thread blocks.  */
+#  define TLS_DTV_AT_TP 1
+#  define TLS_TCB_AT_TP 0
+
+/* Get the thread descriptor definition.  */
+#  include <nptl/descr.h>
+
+typedef struct
+{
+  dtv_t *dtv;
+  void *__private;
+} tcbhead_t;
+
+/* This is the size of the initial TCB.  */
+#  define TLS_INIT_TCB_SIZE sizeof (tcbhead_t)
+
+/* This is the size of the TCB.  */
+#  define TLS_TCB_SIZE sizeof (tcbhead_t)
+
+/* This is the size we need before TCB.  */
+#  define TLS_PRE_TCB_SIZE sizeof (struct pthread)
+
+/* Install the dtv pointer.  The pointer passed is to the element with
+   index -1 which contain the length.  */
+#  define INSTALL_DTV(tcbp, dtvp) (((tcbhead_t *) (tcbp))->dtv = (dtvp) + 1)
+
+/* Install new dtv for current thread.  */
+#  define INSTALL_NEW_DTV(dtv) (THREAD_DTV () = (dtv))
+
+/* Return dtv of given thread descriptor.  */
+#  define GET_DTV(tcbp) (((tcbhead_t *) (tcbp))->dtv)
+
+/* Code to initially initialize the thread pointer.  This might need
+   special attention since 'errno' is not yet available and if the
+   operation can cause a failure 'errno' must not be touched.  */
+#  define TLS_INIT_TP(tcbp)						   \
+    (__builtin_set_thread_pointer ((void *) (tcbp)), true)
+
+/* Value passed to 'clone' for initialization of the thread register.  */
+#  define TLS_DEFINE_INIT_TP(tp, pd) void *tp = (pd) + 1
+
+/* Return the address of the dtv for the current thread.  */
+#  define THREAD_DTV() (((tcbhead_t *) __builtin_thread_pointer ())->dtv)
+
+/* Return the thread descriptor for the current thread.  */
+#  define THREAD_SELF ((struct pthread *) __builtin_thread_pointer () - 1)
+
+/* Magic for libthread_db to know how to do THREAD_SELF.  */
+#  define DB_THREAD_SELF REGISTER (64, 64, 32 * 8, -sizeof (struct pthread))
+
+#  include <tcb-access.h>
+
+/* Get and set the global scope generation counter in struct pthread.  */
+#  define THREAD_GSCOPE_FLAG_UNUSED 0
+#  define THREAD_GSCOPE_FLAG_USED 1
+#  define THREAD_GSCOPE_FLAG_WAIT 2
+#  define THREAD_GSCOPE_RESET_FLAG()					  \
+    do									\
+      {								       \
+	int __res = atomic_exchange_release (				 \
+	    &THREAD_SELF->header.gscope_flag, THREAD_GSCOPE_FLAG_UNUSED);     \
+	if (__res == THREAD_GSCOPE_FLAG_WAIT)				 \
+	  lll_futex_wake (&THREAD_SELF->header.gscope_flag, 1, LLL_PRIVATE);  \
+      }								       \
+    while (0)
+#  define THREAD_GSCOPE_SET_FLAG()					    \
+    do									\
+      {								       \
+	THREAD_SELF->header.gscope_flag = THREAD_GSCOPE_FLAG_USED;	    \
+	atomic_write_barrier ();					      \
+      }								       \
+    while (0)
+
+#endif /* __ASSEMBLER__ */
+
+#endif /* tls.h */
diff --git a/sysdeps/sw_64/stackinfo.h b/sysdeps/sw_64/stackinfo.h
new file mode 100644
index 00000000..661a814a
--- /dev/null
+++ b/sysdeps/sw_64/stackinfo.h
@@ -0,0 +1,33 @@
+/* Copyright (C) 2001-2023 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library.  If not, see
+   <https://www.gnu.org/licenses/>.  */
+
+/* This file contains a bit of information about the stack allocation
+   of the processor.  */
+
+#ifndef _STACKINFO_H
+#define _STACKINFO_H 1
+
+#include <elf.h>
+
+/* On Sw_64 the stack grows down.  */
+#define _STACK_GROWS_DOWN 1
+
+/* Default to an executable stack.  PF_X can be overridden if PT_GNU_STACK is
+ * present, but it is presumed absent.  */
+#define DEFAULT_STACK_PERMS (PF_R | PF_W | PF_X)
+
+#endif /* stackinfo.h */
diff --git a/sysdeps/sw_64/sw8a/nptl/tls.h b/sysdeps/sw_64/sw8a/nptl/tls.h
new file mode 100644
index 00000000..d0942c19
--- /dev/null
+++ b/sysdeps/sw_64/sw8a/nptl/tls.h
@@ -0,0 +1,106 @@
+/* Definition for thread-local data handling.  NPTL/Sw_64 version.
+   Copyright (C) 2003-2023 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library.  If not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#ifndef _TLS_H
+#define _TLS_H 1
+
+#include <dl-sysdep.h>
+
+#ifndef __ASSEMBLER__
+#  include <stdbool.h>
+#  include <stddef.h>
+#  include <stdint.h>
+#  include <dl-dtv.h>
+
+/* Get system call information.  */
+#  include <sysdep.h>
+
+/* The TP points to the start of the thread blocks.  */
+#  define TLS_DTV_AT_TP 1
+#  define TLS_TCB_AT_TP 0
+
+/* Get the thread descriptor definition.  */
+#  include <nptl/descr.h>
+
+typedef struct
+{
+  dtv_t *dtv;
+  void *__private;
+} tcbhead_t;
+
+/* This is the size of the initial TCB.  */
+#  define TLS_INIT_TCB_SIZE sizeof (tcbhead_t)
+
+/* This is the size of the TCB.  */
+#  define TLS_TCB_SIZE sizeof (tcbhead_t)
+
+/* This is the size we need before TCB.  */
+#  define TLS_PRE_TCB_SIZE sizeof (struct pthread)
+
+/* Install the dtv pointer.  The pointer passed is to the element with
+   index -1 which contain the length.  */
+#  define INSTALL_DTV(tcbp, dtvp) (((tcbhead_t *) (tcbp))->dtv = (dtvp) + 1)
+
+/* Install new dtv for current thread.  */
+#  define INSTALL_NEW_DTV(dtv) (THREAD_DTV () = (dtv))
+
+/* Return dtv of given thread descriptor.  */
+#  define GET_DTV(tcbp) (((tcbhead_t *) (tcbp))->dtv)
+
+/* Code to initially initialize the thread pointer.  This might need
+   special attention since 'errno' is not yet available and if the
+   operation can cause a failure 'errno' must not be touched.  */
+#  define TLS_INIT_TP(tcbp)						   \
+    (__builtin_set_thread_pointer ((void *) (tcbp)), true)
+
+/* Value passed to 'clone' for initialization of the thread register.  */
+#  define TLS_DEFINE_INIT_TP(tp, pd) void *tp = (pd) + 1
+
+/* Return the address of the dtv for the current thread.  */
+#  define THREAD_DTV() (((tcbhead_t *) __builtin_thread_pointer ())->dtv)
+
+/* Return the thread descriptor for the current thread.  */
+#  define THREAD_SELF ((struct pthread *) __builtin_thread_pointer () - 1)
+
+/* Magic for libthread_db to know how to do THREAD_SELF.  */
+#  define DB_THREAD_SELF REGISTER (64, 64, 32 * 8, -sizeof (struct pthread))
+
+#  include <tcb-access.h>
+#  define THREAD_GSCOPE_FLAG_UNUSED 0
+#  define THREAD_GSCOPE_FLAG_USED 1
+#  define THREAD_GSCOPE_FLAG_WAIT 2
+#  define THREAD_GSCOPE_RESET_FLAG()					  \
+    do									\
+      {								       \
+	int __res = atomic_exchange_rel (&THREAD_SELF->header.gscope_flag,    \
+					 THREAD_GSCOPE_FLAG_UNUSED);	  \
+	if (__res == THREAD_GSCOPE_FLAG_WAIT)				 \
+	  lll_futex_wake (&THREAD_SELF->header.gscope_flag, 1, LLL_PRIVATE);  \
+      }								       \
+    while (0)
+#  define THREAD_GSCOPE_SET_FLAG()					    \
+    do									\
+      {								       \
+	THREAD_SELF->header.gscope_flag = THREAD_GSCOPE_FLAG_USED;	    \
+	atomic_write_barrier ();					      \
+      }								       \
+    while (0)
+
+#endif /* __ASSEMBLER__ */
+
+#endif /* tls.h */
-- 
2.25.1

